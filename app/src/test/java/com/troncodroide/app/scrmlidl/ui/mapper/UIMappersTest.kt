package com.troncodroide.app.scrmlidl.ui.mapper

import com.troncodroide.app.scrmlidl.R
import com.troncodroide.app.scrmlidl.defaultDSLCoupon
import com.troncodroide.app.scrmlidl.defaultDetailUi
import com.troncodroide.app.scrmlidl.defaultDomainCoupon
import com.troncodroide.app.scrmlidl.domain.models.Coupon
import com.troncodroide.app.scrmlidl.framework.providers.CalendarEvaluatorImpl
import com.troncodroide.app.scrmlidl.framework.providers.ResProvider
import com.troncodroide.app.scrmlidl.ui.models.TextIconItemUI
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date

class UIMappersTest {

    private val resProvider = Mockito.mock(ResProvider::class.java)

    @Before
    fun mockDefaultTexts() {
        Mockito.`when`(resProvider.getString(anyString())).thenReturn("Text")
        Mockito.`when`(resProvider.getString(anyInt())).thenReturn("Text")
        Mockito.`when`(resProvider.getString(R.string.text_outdated_coupon))
            .thenReturn("text_outdated_coupon")
        Mockito.`when`(resProvider.getString(R.string.text_on_units)).thenReturn("text_on_units")
        Mockito.`when`(resProvider.getString(R.string.text_coupon_title))
            .thenReturn("text_coupon_title")
        Mockito.`when`(resProvider.getString(R.string.text_disabled)).thenReturn("text_disabled")
        Mockito.`when`(resProvider.getString(R.string.text_discount)).thenReturn("text_discount")
        Mockito.`when`(resProvider.getString(R.string.text_enabled)).thenReturn("text_enabled")
        Mockito.`when`(resProvider.getString(R.string.text_last_day))
            .thenReturn("text_last_day")
        Mockito.`when`(resProvider.getString(R.string.text_sufix_days_left))
            .thenReturn("text_sufix_days_left")
        Mockito.`when`(resProvider.getString(R.string.text_sufix_not_started))
            .thenReturn("text_sufix_not_started")
        Mockito.`when`(resProvider.getString(R.string.text_v_code)).thenReturn("text_v_code")
    }

    @Test
    fun testCalendarDays() {
        val today = "TODAY".toTime()
        val todayMorning = "TODAYMORNING".toTime()
        val todayNight = "TODAYNIGHT".toTime()
        val tomorrow = "TOMORROW".toTime()
        val yesterday = "YESTERDAY".toTime()

        Assert.assertTrue(today in todayMorning..todayNight)
        Assert.assertTrue(todayMorning < todayNight)
        Assert.assertTrue(today > yesterday)
        Assert.assertTrue(yesterday < tomorrow)
        Assert.assertTrue(today < tomorrow)
    }

    fun Long.toDateString(): String = SimpleDateFormat("dd/MM/yyyy HH:MM").format(Date(this))

    @Test
    fun testDetailUI() {
        val calendarEvaluator = MockCalendarImpl(20)
        val detailUI = UIMappers.DetailMapper.mapDetailUI(resProvider, defaultDomainCoupon(), calendarEvaluator)
        Assert.assertEquals(defaultDetailUi(), detailUI)
    }

    @Test
    fun testCalendarMapperOfferStartedYesterdayEndsYesterday() {
        val customDomain = defaultDSLCoupon().apply {
            startDate = "YESTERDAY"
            endDate = "YESTERDAY"
        }.toDomain()

        val detailUI = textIconItemUI(customDomain)
        Assert.assertEquals(detailUI!!.text.text, "text_outdated_coupon")
    }

    @Test
    fun testCalendarMapperOfferStartedYesterdayEndsToday() {
        val icontext = defaultDSLCoupon().apply {
            startDate = "YESTERDAY"
            endDate = "TODAYNIGHT"
        }.toDomain().let(::textIconItemUI)
        Assert.assertEquals("text_last_day", icontext!!.text.text)
    }

    @Test
    fun testCalendarMapperOfferStartedYesterdayEndsTomorrow() {
        val icontext = defaultDSLCoupon().apply {
            startDate = "YESTERDAY"
            endDate = "TOMORROW"
        }.toDomain().let(::textIconItemUI)
        Assert.assertEquals("text_last_day", icontext!!.text.text)
    }

    @Test
    fun testCalendarMapperOfferStartsTodayEndsYesterday() {
        val iconText = defaultDSLCoupon().apply {
            startDate = "TODAY"
            endDate = "YESTERDAY"

        }.toDomain().let(::textIconItemUI)
        Assert.assertEquals(null, iconText)
    }

    @Test
    fun testCalendarMapperOfferStartsTodayEndsToday() {
        val iconText = defaultDSLCoupon().apply {
            startDate = "TODAYMORNING"
            endDate = "TODAYNIGHT"
        }.toDomain().let(::textIconItemUI)
        Assert.assertEquals("text_last_day", iconText!!.text.text)
    }

    @Test
    fun testCalendarMapperOfferStartsTodayEndsTomorrow() {
        val iconText = defaultDSLCoupon().apply {
            startDate = "TODAYMORNING"
            endDate = "TOMORROW"
        }.toDomain().let(::textIconItemUI)
        Assert.assertEquals("text_last_day", iconText!!.text.text)
    }

    private fun textIconItemUI(customDomain: Coupon): TextIconItemUI? {
        val calendarEavaluator = MockCalendarImpl(12)
        val detailUI = UIMappers.DetailMapper.mapDetailUI(resProvider, customDomain, calendarEavaluator)
            .items.firstOrNull { it is TextIconItemUI } as? TextIconItemUI
        return detailUI
    }
}

private class MockCalendarImpl(private val hourOfDay: Int) : CalendarEvaluatorImpl() {
    override fun getCalendar(): Calendar =
        Calendar.getInstance().apply { set(Calendar.HOUR_OF_DAY, hourOfDay) }
}