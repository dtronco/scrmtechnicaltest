package com.troncodroide.app.scrmlidl

import com.troncodroide.app.scrmlidl.domain.models.Amount
import com.troncodroide.app.scrmlidl.domain.models.Limitation
import com.troncodroide.app.scrmlidl.domain.models.Product
import com.troncodroide.app.scrmlidl.domain.models.Profit
import java.io.Serializable
import com.troncodroide.app.scrmlidl.domain.models.Coupon as DCoupon

fun coupon(block: DSLCoupon.() -> Unit): DSLCoupon = DSLCoupon().apply(block)

fun DSLCoupon.product(block: DSLProduct.() -> Unit) {
    product = DSLProduct().apply(block)
}

fun DSLCoupon.profit(block: DSLProfit.() -> Unit) {
    profit = DSLProfit().apply(block)
}

fun DSLCoupon.limitation(block: DSLLimitation.() -> Unit) {
    limitation = DSLLimitation().apply(block)
}

data class DSLCoupon(
    var id: String? = null,
    var product: DSLProduct? = null,
    var profit: DSLProfit? = null,
    var startDate: String? = null,
    var endDate: String? = null,
    var enabled: Boolean? = null,
    var limitation: DSLLimitation? = null
) {
    fun toDomain(): DCoupon =
        DCoupon(
            id = id.orEmpty(),
            limitation = limitation!!.toDomain(),
            product = product!!.toDomain(),
            profit = profit!!.toDomain(),
            startDate = startDate.orEmpty(),
            endDate = endDate.orEmpty(),
            enabled = enabled ?: false)
}

data class DSLProduct(
    var id: String? = null,
    var title: String? = null,
    var description: String? = null,
    var image: String? = null,
    var logo: String? = null,
    var background: String? = null
) : Serializable {
    fun toDomain(): Product = Product(
        id = id.orEmpty(),
        image = image.orEmpty(),
        description = description.orEmpty(),
        title = title.orEmpty(),
        background = background.orEmpty(),
        logo = logo.orEmpty()
    )
}

data class DSLLimitation(
    var items: Int? = null,
    var maxAmount: DSLAmount? = null,
    var minAmount: DSLAmount? = null,
    var maxUses: Int? = null
) : Serializable {
    fun toDomain(): Limitation =
        Limitation(
            items = items,
            maxAmount = maxAmount?.toDomain(),
            maxUses = maxUses,
            minAmount = minAmount?.toDomain()
        )
}

data class DSLProfit(
    var id: String? = null,
    var unit: Int? = null,
    var percentage: Float? = null
) : Serializable {
    fun toDomain(): Profit =
        Profit(
            id = id.orEmpty(),
            percentage = percentage ?: 0f,
            unit = unit ?: 0
        )
}

data class DSLAmount(
    var money: Float? = null,
    var currency: String? = null
) : Serializable {
    fun toDomain(): Amount = Amount(
        money = money ?: 0f,
        currency = currency.orEmpty()
    )
}
