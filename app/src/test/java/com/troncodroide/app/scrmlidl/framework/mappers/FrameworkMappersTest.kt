package com.troncodroide.app.scrmlidl.framework.mappers

import com.troncodroide.app.scrmlidl.defaultDomainCoupon
import com.troncodroide.app.scrmlidl.defaultFrameworkCoupon
import org.junit.Assert
import org.junit.Test

class FrameworkMappersTest {

    @Test
    fun testMapToDomain() {
        val coupon = FrameworkMappers.mapToDomain(defaultFrameworkCoupon())
        Assert.assertEquals(coupon, defaultDomainCoupon())
    }
}
