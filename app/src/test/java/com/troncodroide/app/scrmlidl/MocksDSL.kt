package com.troncodroide.app.scrmlidl

import com.troncodroide.app.scrmlidl.ui.coupons.CouponDetailUi
import com.troncodroide.app.scrmlidl.ui.models.SwitchItemUI
import com.troncodroide.app.scrmlidl.ui.models.TextColor
import com.troncodroide.app.scrmlidl.ui.models.TextIconItemUI
import com.troncodroide.app.scrmlidl.ui.models.TextSize
import com.troncodroide.app.scrmlidl.ui.models.TextUi
import com.troncodroide.app.scrmlidl.ui.models.ThreeLinesItemUI
import com.troncodroide.app.scrmlidl.ui.models.TwoLinesItemUI
import com.troncodroide.app.scrmlidl.domain.models.Coupon as DCoupon
import com.troncodroide.app.scrmlidl.framework.models.Coupon as FCoupon

fun defaultDSLCoupon(): DSLCoupon {
    return coupon {
        id = "ID"
        enabled = false
        endDate = "TOMORROW"
        startDate = "TODAYMORNING"
        profit {
            this.id = "3%100"
            percentage = 1.0f
            unit = 3
        }
        product {
            this.id = "PID"
            image = "PIMAGE"
            background = "PBG"
            description = "Subtitle"
            logo = "PLOGO"
            title = "Title"

        }
        limitation {
            items = 3
        }
    }
}

fun defaultDomainCoupon(): DCoupon {
    return defaultDSLCoupon().toDomain()
}

fun defaultFrameworkCoupon(): FCoupon {
    return FCoupon(
        id = "ID",
        productBackground = "PBG",
        profit = "3%100",
        startDate = "TODAYMORNING",
        endDate = "TOMORROW",
        enabled = true,
        limitations = "MAXQTY:3",
        productBrand = "PBRAND",
        productid = "PID",
        productImage = "PIMAGE",
        productLogo = "PLOGO",
        productSubTitle = "Subtitle",
        productTitle = "Title"
    )
}

fun defaultDetailUi(): CouponDetailUi {
    return CouponDetailUi(
        title = "Title",
        couponID = "ID",
        image = "PBG",
        items = listOf(
            SwitchItemUI(estableId = "ID".hashCode().toLong(), couponID = "ID", title = TextUi(text = "text_coupon_title", textSize = TextSize.M, textColor = TextColor.PRIMARY), subtitle = TextUi(text = "text_disabled", textSize = TextSize.S, textColor = TextColor.SECONDARY), selected = false),
            TextIconItemUI(estableId = "ID".hashCode().toLong(), text = TextUi(text = "text_last_day", textSize = TextSize.M, textColor = TextColor.PRIMARY), icon = android.R.drawable.ic_menu_my_calendar),
            TwoLinesItemUI(estableId = "ID".hashCode().toLong(), couponID = "ID", title = TextUi(text = "text_v_code", textSize = TextSize.M, textColor = TextColor.PRIMARY), subtitle = TextUi(text = "ID", textSize = TextSize.S, textColor = TextColor.SECONDARY)),
            ThreeLinesItemUI(estableId = "PID".hashCode().toLong(), couponID = "PID", title = TextUi(text = "Title", textSize = TextSize.S, textColor = TextColor.PRIMARY), subtitle = TextUi(text = "Subtitle", textSize = TextSize.S, textColor = TextColor.SECONDARY), category = TextUi(text = "PID", textSize = TextSize.S, textColor = TextColor.ACCENT)),
            ThreeLinesItemUI(estableId = "3%100".hashCode().toLong(), couponID = "3%100", title = TextUi(text = "3 X 2", textSize = TextSize.S, textColor = TextColor.PRIMARY), subtitle = TextUi(text = "", textSize = TextSize.S, textColor = TextColor.SECONDARY), category = TextUi(text = "3%100", textSize = TextSize.S, textColor = TextColor.ACCENT)))
    )
}

