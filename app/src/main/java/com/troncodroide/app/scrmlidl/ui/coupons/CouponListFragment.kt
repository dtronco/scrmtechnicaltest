package com.troncodroide.app.scrmlidl.ui.coupons

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import com.troncodroide.app.core.either
import com.troncodroide.app.scrmlidl.R
import com.troncodroide.app.scrmlidl.ui.coupons.widget.VerticalSpaceItemDecoration
import com.troncodroide.app.scrmlidl.ui.coupons.widget.couponAdapterDelegate
import com.troncodroide.app.scrmlidl.ui.extensions.visible
import com.troncodroide.app.scrmlidl.ui.models.CouponItemUI
import com.troncodroide.app.scrmlidl.ui.models.EstableItem
import kotlinx.android.synthetic.main.fragment_coupons_list.*
import org.koin.android.viewmodel.ext.android.viewModel

class CouponListFragment : Fragment() {

    private val viewModel: CouponListViewModel by viewModel()
    private val adapter = ListDelegationAdapter<List<EstableItem>>(
        couponAdapterDelegate(
            { couponId, transitionViews -> openDetailFragment(couponId, transitionViews) },
            { couponId, enabled -> activeCoupon(couponId, enabled) }
        )
    )

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_coupons_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        observeLiveDatas()
        viewModel.getData()
    }

    private fun initViews() {
        (activity as? AppCompatActivity)?.setSupportActionBar(coupon_list_toolbar)
        coupon_list_recycler.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        coupon_list_recycler.addItemDecoration(VerticalSpaceItemDecoration(resources.getDimensionPixelOffset(R.dimen.margin_m)))
        coupon_list_recycler.adapter = adapter
    }

    private fun observeLiveDatas() {
        viewModel.listLiveData.observe(viewLifecycleOwner, Observer { data ->
            data?.either(this::handleError, this::handleSuccess)
        })
        viewModel.fullLoadingLiveData.observe(viewLifecycleOwner, Observer { data ->
            data?.let(::handleLoading)
        })
    }

    // Interactions

    private fun handleLoading(isLoading: Boolean) {
        coupon_list_loading.visible(isLoading)
    }

    private fun handleSuccess(data: List<CouponItemUI>) {

        adapter.items = data
        val controller = AnimationUtils.loadLayoutAnimation(coupon_list_recycler.context, R.anim.layout_animation_fall_down)
        coupon_list_recycler.layoutAnimation = controller
        coupon_list_recycler.adapter?.notifyDataSetChanged()
        coupon_list_recycler.scheduleLayoutAnimation()
    }

    private fun handleError(error: Throwable) {
        Log.e("Error", error.message, error)
    }

    // Actions
    private fun openDetailFragment(couponId: String, transitions: Array<Pair<View, String>>) {
        val extras = FragmentNavigatorExtras(
            *transitions
        )
        findNavController().navigate(
            CouponListFragmentDirections.actionCouponListFragmentToCouponDetailFragment(couponId), extras)
    }

    private fun activeCoupon(couponId: String, enabled: Boolean) {
        viewModel.switchActive(couponId, enabled)
    }
}
