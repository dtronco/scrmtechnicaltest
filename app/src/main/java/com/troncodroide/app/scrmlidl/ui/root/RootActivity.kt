package com.troncodroide.app.scrmlidl.ui.root

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.troncodroide.app.scrmlidl.R

class RootActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_root)
    }
}
