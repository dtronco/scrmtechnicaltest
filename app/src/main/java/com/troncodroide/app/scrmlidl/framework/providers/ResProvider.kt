package com.troncodroide.app.scrmlidl.framework.providers

import android.content.Context
import androidx.annotation.StringRes

open class ResProviderImpl(private val context: Context) : ResProvider {

    override fun getString(res: Int, vararg formatArgs: Any): String {
        return context.getString(res, formatArgs)
    }

    override fun getString(key: String, vararg formatArgs: Any): String {
        return context.getString(
            context.resources.getIdentifier(key, "string", context.packageName),
            formatArgs
        )
    }
}

interface ResProvider {
    fun getString(@StringRes res: Int, vararg formatArgs: Any): String
    fun getString(key: String, vararg formatArgs: Any): String
}