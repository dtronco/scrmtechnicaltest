package com.troncodroide.app.scrmlidl.framework

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.troncodroide.app.core.Either
import com.troncodroide.app.scrmlidl.data.contracts.NetworkDatasource
import com.troncodroide.app.scrmlidl.framework.mappers.FrameworkMappers
import java.io.InputStreamReader
import com.troncodroide.app.scrmlidl.domain.models.Coupon as DomainCoupon
import com.troncodroide.app.scrmlidl.framework.models.Coupon as FrameworkCoupon

class FakeNetworkFromAssetsDS(private val context: Context) : NetworkDatasource {
    override fun getData(page: Int): Either<Throwable, List<DomainCoupon>> {
        val reader = InputStreamReader(context.assets.open("coupons.json"))
        val token = object : TypeToken<List<FrameworkCoupon>>() {}.type
        val frameworkcoupons = Gson().fromJson<List<FrameworkCoupon>>(reader, token)
        return Either.Right(frameworkcoupons.map(FrameworkMappers::mapToDomain))
    }
}