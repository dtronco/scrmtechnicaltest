package com.troncodroide.app.scrmlidl.ui.coupons

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.troncodroide.app.core.Either
import com.troncodroide.app.core.map
import com.troncodroide.app.scrmlidl.framework.providers.ResProvider
import com.troncodroide.app.scrmlidl.ui.mapper.UIMappers
import com.troncodroide.app.scrmlidl.ui.models.CouponItemUI
import com.troncodroide.app.scrmlidl.usecases.GetCoupons
import com.troncodroide.app.scrmlidl.usecases.SetActiveCoupon
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class CouponListViewModel(private val goupons: GetCoupons, private val activeCoupon: SetActiveCoupon, private val resProvider: ResProvider) : ViewModel() {
    val listLiveData: MutableLiveData<Either<Throwable, List<CouponItemUI>>?> = MutableLiveData()
    val fullLoadingLiveData: MutableLiveData<Boolean> = MutableLiveData()

    fun getData() = GlobalScope.launch(Dispatchers.Main) {
        fullLoadingLiveData.value = true
        val data = goupons()
        fullLoadingLiveData.value = false
        listLiveData.value = data.map { domainList ->
            domainList.map { UIMappers.ListMapper.mapItemUI(resProvider, it) }
        }
    }.start()

    fun switchActive(couponId: String, active: Boolean) = GlobalScope.launch(Dispatchers.Main) {
        activeCoupon(couponId to active)
    }.start()
}