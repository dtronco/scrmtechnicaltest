package com.troncodroide.app.scrmlidl.framework

import android.content.Context
import com.troncodroide.app.scrmlidl.data.contracts.PersistenceDatasource
import java.util.Locale

class PersistenceDS(context: Context) : PersistenceDatasource {

    private val sharedPreferences = context.getSharedPreferences("ActiveCoupons", Context.MODE_PRIVATE)

    override fun isActive(id: String): Boolean {
        return sharedPreferences.getBoolean(id.getAsKey(), false)
    }

    override fun setActive(id: String, active: Boolean) {
        sharedPreferences.edit().putBoolean(id.getAsKey(), active).apply()
    }

    private fun String.getAsKey(): String {
        return "COUPON_${this.toUpperCase(Locale.getDefault())}"
    }
}