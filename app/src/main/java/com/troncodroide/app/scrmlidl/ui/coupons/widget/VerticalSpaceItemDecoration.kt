package com.troncodroide.app.scrmlidl.ui.coupons.widget

import android.graphics.Rect
import androidx.recyclerview.widget.RecyclerView

class VerticalSpaceItemDecoration(private val verticalSpaceHeight: Int) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect, itemPosition: Int, parent: RecyclerView) {
        if (itemPosition != (parent.adapter?.itemCount?.minus(1)))
            outRect.bottom = verticalSpaceHeight
    }
}