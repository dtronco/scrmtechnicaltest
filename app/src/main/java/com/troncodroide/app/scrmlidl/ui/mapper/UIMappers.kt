package com.troncodroide.app.scrmlidl.ui.mapper

import com.troncodroide.app.scrmlidl.R
import com.troncodroide.app.scrmlidl.domain.models.Coupon
import com.troncodroide.app.scrmlidl.framework.providers.CalendarEavaluator
import com.troncodroide.app.scrmlidl.framework.providers.ResProvider
import com.troncodroide.app.scrmlidl.framework.providers.Status
import com.troncodroide.app.scrmlidl.ui.coupons.CouponDetailUi
import com.troncodroide.app.scrmlidl.ui.models.CouponItemUI
import com.troncodroide.app.scrmlidl.ui.models.EstableItem
import com.troncodroide.app.scrmlidl.ui.models.SwitchItemUI
import com.troncodroide.app.scrmlidl.ui.models.TextIconItemUI
import com.troncodroide.app.scrmlidl.ui.models.TextUi
import com.troncodroide.app.scrmlidl.ui.models.ThreeLinesItemUI
import com.troncodroide.app.scrmlidl.ui.models.TwoLinesItemUI
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale
import java.util.concurrent.TimeUnit

object UIMappers {
    object DetailMapper {

        fun mapDetailUI(resProvider: ResProvider, coupon: Coupon, calendarEavaluator: CalendarEavaluator): CouponDetailUi {

            return CouponDetailUi(
                couponID = coupon.id,
                title = coupon.product.title,
                image = coupon.product.background,
                items = mapDetailList(resProvider, coupon, calendarEavaluator)
            )
        }

        fun mapDetailList(resProvider: ResProvider, coupon: Coupon, calendarEavaluator: CalendarEavaluator): List<EstableItem> {
            val toRet = mutableListOf<EstableItem>()

            toRet.add(SwitchItemUI(
                couponID = coupon.id,
                estableId = coupon.id.hashCode().toLong(),
                title = TextUi.title(resProvider.getString(R.string.text_coupon_title)),
                subtitle = TextUi.secondary(coupon.enabled.toText(
                    resProvider.getString(R.string.text_enabled),
                    resProvider.getString(R.string.text_disabled))),
                selected = coupon.enabled
            ))

            val today = Calendar.getInstance().time.time
            val startDateTime = coupon.startDate.toTime()
            val endDateTime = coupon.endDate.toTime()
            val status = calendarEavaluator.evaluate(startDateTime, endDateTime)
            when (status) {
                Status.OUTDATED -> {
                    toRet.add(TextIconItemUI(
                        estableId = coupon.id.hashCode().toLong(),
                        text = TextUi.title(resProvider.getString(R.string.text_outdated_coupon)),
                        icon = android.R.drawable.ic_menu_my_calendar
                    ))
                }
                Status.NOT_STARTED -> {
                    toRet.add(TextIconItemUI(
                        estableId = coupon.id.hashCode().toLong(),
                        text = TextUi.title(startDateTime.diffDays(today).toString() + resProvider.getString(R.string.text_sufix_not_started)),
                        icon = android.R.drawable.ic_menu_my_calendar
                    ))
                }
                Status.STARTED -> {
                    val text = if (startDateTime.diffDays(today) == 1L) {
                        TextUi.title(startDateTime.diffDays(today).toString() + resProvider.getString(R.string.text_sufix_days_left))
                    } else {
                        TextUi.title(resProvider.getString(R.string.text_last_day))
                    }
                    toRet.add(TextIconItemUI(
                        estableId = coupon.id.hashCode().toLong(),
                        text = text,
                        icon = android.R.drawable.ic_menu_my_calendar))
                }
                Status.NOTVALID -> Unit
            }

            toRet.add(TwoLinesItemUI(
                couponID = coupon.id,
                estableId = coupon.id.hashCode().toLong(),
                title = TextUi.title(resProvider.getString(R.string.text_v_code)),
                subtitle = TextUi.secondary(coupon.id)
            ))

            toRet.add(ThreeLinesItemUI(
                couponID = coupon.product.id,
                estableId = coupon.product.id.hashCode().toLong(),
                title = TextUi.default(coupon.product.title),
                subtitle = TextUi.secondary(coupon.product.description),
                category = TextUi.accent(coupon.product.id)
            ))

            toRet.add(ThreeLinesItemUI(
                couponID = coupon.profit.id,
                estableId = coupon.profit.id.hashCode().toLong(),
                title = TextUi.default(ListMapper.getProfitTitle(coupon)),
                subtitle = TextUi.secondary(ListMapper.getProfitSubtitle(resProvider, coupon)),
                category = TextUi.accent(coupon.profit.id)
            ))

            return toRet
        }

        private fun Boolean.toText(trueText: String, falseText: String): String {
            return if (this) trueText else falseText
        }
    }

    object ListMapper {
        fun mapItemUI(resProvider: ResProvider, coupon: Coupon): CouponItemUI {

            return CouponItemUI(
                couponID = coupon.id,
                productTitle = coupon.product.title,
                productSubtitle = coupon.product.description,
                productImageUrl = coupon.product.image,
                productLogoImageUrl = coupon.product.logo,
                productBackground = coupon.product.background,
                profitTitle = getProfitTitle(coupon),
                profitSubtitle = getProfitSubtitle(resProvider, coupon),
                acceptButtonText = resProvider.getString(R.string.item_ui_accept_button),
                isChecked = coupon.enabled,
                estableId = coupon.id.hashCode().toLong()
            )
        }

        fun getProfitTitle(coupon: Coupon): String {

            return when {
                coupon.profit.percentage == 1.0f -> "${coupon.profit.unit} X ${coupon.profit.unit - 1}"
                coupon.profit.unit == 1 -> "${(coupon.profit.percentage * 100).toInt()}%"
                else -> "${(coupon.profit.percentage * 100).toInt()}%"
            }
        }

        fun getProfitSubtitle(res: ResProvider, coupon: Coupon): String {

            return when {
                coupon.profit.percentage == 1.0f -> ""
                coupon.profit.unit == 1 -> res.getString(R.string.text_discount)
                else -> res.getString(R.string.text_on_units, coupon.profit.unit)

            }
        }
    }
}

fun String.toTime(): Long {
    val time = when {
        startsWith("+") -> dayToMillis()
        startsWith("-") -> dayToMillis()
        equals("TODAY") -> Calendar.getInstance().apply { set(Calendar.HOUR_OF_DAY, 13) }.time.time
        equals("TODAYNIGHT") -> Calendar.getInstance().apply { set(Calendar.HOUR_OF_DAY, 20) }.time.time
        equals("TODAYMORNING") -> Calendar.getInstance().apply { set(Calendar.HOUR_OF_DAY, 8) }.time.time
        equals("TOMORROW") -> "+1".dayToMillis()
        equals("YESTERDAY") -> "-1".dayToMillis()
        else -> SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(this)?.time

    }
    return time ?: throw IllegalStateException()
}

fun String.dayToMillis(): Long {
    val c = Calendar.getInstance().apply { set(Calendar.HOUR_OF_DAY, 13) }
    if (this.startsWith("+")) {
        val days = this.removePrefix("+").toInt()
        c.add(Calendar.DAY_OF_YEAR, days)
    } else if (this.startsWith("-")) {
        val days = this.removePrefix("-").toInt()
        c.add(Calendar.DAY_OF_YEAR, -1 * days)
    }

    return c.time.time
}

private fun Long.diffDays(time: Long): Long {
    return TimeUnit.MILLISECONDS.toDays(time - this)
}
