package com.troncodroide.app.scrmlidl.framework.providers

import java.util.Calendar

interface CalendarEavaluator {
    fun getCalendar(): Calendar
    fun evaluate(startedTime: Long, endTime: Long): Status
}

open class CalendarEvaluatorImpl : CalendarEavaluator {
    override fun getCalendar(): Calendar = Calendar.getInstance()

    override fun evaluate(startedTime: Long, endTime: Long): Status {
        val today = getCalendar().timeInMillis
        return when {
            startedTime > endTime -> Status.NOTVALID
            today > endTime -> Status.OUTDATED
            today < startedTime -> Status.NOT_STARTED
            today in startedTime..endTime -> Status.STARTED
            else -> Status.NOTVALID
        }
    }
}

enum class Status {
    OUTDATED, NOT_STARTED, STARTED, NOTVALID
}