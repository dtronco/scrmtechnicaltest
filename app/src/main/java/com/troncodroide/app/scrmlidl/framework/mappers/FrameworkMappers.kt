package com.troncodroide.app.scrmlidl.framework.mappers

import com.troncodroide.app.scrmlidl.domain.models.Amount
import com.troncodroide.app.scrmlidl.domain.models.Limitation
import com.troncodroide.app.scrmlidl.domain.models.Product
import com.troncodroide.app.scrmlidl.domain.models.Profit
import com.troncodroide.app.scrmlidl.domain.models.Coupon as DCoupon
import com.troncodroide.app.scrmlidl.framework.models.Coupon as FCoupon

object FrameworkMappers {
    fun mapToDomain(fcoupon: FCoupon): DCoupon {
        return DCoupon(
            id = fcoupon.id,
            enabled = false,
            product = Product(
                id = fcoupon.productid,
                title = fcoupon.productTitle,
                description = fcoupon.productSubTitle,
                image = fcoupon.productImage,
                logo = fcoupon.productLogo,
                background = fcoupon.productBackground
            ),
            endDate = fcoupon.endDate,
            startDate = fcoupon.startDate,
            profit = mapProfit(fcoupon.profit),
            limitation = mapLimitation(fcoupon.limitations)
        )
    }

    private fun mapProfit(profitId: String): Profit {

        val profitType = if (profitId.contains("%")) {
            val splitted = profitId.split("%")
            splitted[0].toInt() to splitted[1].toFloat() / 100f
        } else {
            0 to 0f
        }

        return Profit(profitId, profitType.first, profitType.second)
    }

    private fun mapLimitation(limitation: String): Limitation {
        val parsed = limitation.split("|")
        val maxqty = parsed.getLimitationKey("MAXQTY")?.toIntOrNull()
        val maxuses = parsed.getLimitationKey("MAXUSES")?.toIntOrNull()
        val maxAmount = parsed.getLimitationKey("MAXAMOUNT")?.toFloatOrNull()
        val minAmount = parsed.getLimitationKey("MINAMOUNT")?.toFloatOrNull()
        val currency = parsed.getLimitationKey("CURRENCY")

        val amountMax = maxAmount?.takeIf { currency != null }?.let { Amount(it, currency!!) }
        val amountMin = minAmount?.takeIf { currency != null }?.let { Amount(it, currency!!) }

        return Limitation(maxqty, amountMax, amountMin, maxuses)
    }

    private fun List<String>.getLimitationKey(key: String): String? {
        return firstOrNull { it.startsWith(key) }?.split(":")?.getOrNull(1)
    }
}