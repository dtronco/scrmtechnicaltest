package com.troncodroide.app.scrmlidl.ui.models

import com.troncodroide.app.scrmlidl.domain.models.Product

data class ProductItemUI(
    override val estableId: Long,
    val couponID: String,
    val productTitle: String,
    val productSubtitle: String,
    val productBrand: String,
    val product: Product
) : EstableItem(estableId)
