package com.troncodroide.app.scrmlidl

import android.app.Application
import com.troncodroide.app.scrmlidl.di.modules
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class CPApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@CPApplication)
            modules(modules)
        }
    }
}