package com.troncodroide.app.scrmlidl.ui.models

data class CouponItemUI(
    override val estableId: Long,
    val couponID: String,
    val productTitle: String,
    val productSubtitle: String,
    val productBackground: String,
    val productImageUrl: String,
    val productLogoImageUrl: String,
    val acceptButtonText: String,
    val profitTitle: String,
    val profitSubtitle: String,
    val isChecked: Boolean
) : EstableItem(estableId)
