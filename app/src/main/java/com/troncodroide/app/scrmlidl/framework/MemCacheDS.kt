package com.troncodroide.app.scrmlidl.framework

import com.troncodroide.app.core.Either
import com.troncodroide.app.scrmlidl.data.contracts.MemDatasource
import com.troncodroide.app.scrmlidl.domain.models.Coupon

class MemCacheDS : MemDatasource {

    private var cache: MutableMap<Int, List<Coupon>> = mutableMapOf()

    override fun hasData(page: Int): Boolean {
        return cache[page] != null
    }

    override fun getData(page: Int): Either<Throwable, List<Coupon>> {
        return Either.Right(cache[page] ?: error("Cache error"))
    }

    override fun setData(page: Int, data: List<Coupon>) {
        cache[page] = data
    }
}