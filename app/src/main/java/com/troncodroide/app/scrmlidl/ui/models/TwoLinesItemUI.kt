package com.troncodroide.app.scrmlidl.ui.models

data class TwoLinesItemUI(
    override val estableId: Long,
    val couponID: String,
    val title: TextUi,
    val subtitle: TextUi
) : EstableItem(estableId)
