package com.troncodroide.app.scrmlidl.ui.coupons

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.transition.TransitionInflater
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import com.troncodroide.app.core.either
import com.troncodroide.app.scrmlidl.R
import com.troncodroide.app.scrmlidl.framework.extensions.loadImage
import com.troncodroide.app.scrmlidl.ui.coupons.widget.VerticalSpaceItemDecoration
import com.troncodroide.app.scrmlidl.ui.coupons.widget.switchAdapterDelegate
import com.troncodroide.app.scrmlidl.ui.coupons.widget.textIconAdapterDelegate
import com.troncodroide.app.scrmlidl.ui.coupons.widget.threeLinesAdapterDelegate
import com.troncodroide.app.scrmlidl.ui.coupons.widget.twoLinesAdapterDelegate
import com.troncodroide.app.scrmlidl.ui.extensions.visible
import com.troncodroide.app.scrmlidl.ui.models.EstableItem
import kotlinx.android.synthetic.main.fragment_coupon_detail.*
import org.koin.android.viewmodel.ext.android.viewModel

class CouponDetailFragment : Fragment() {

    private val viewModel: CouponDetailViewModel by viewModel()
    private val args: CouponDetailFragmentArgs by navArgs()

    private val adapter = ListDelegationAdapter<List<EstableItem>>(
        twoLinesAdapterDelegate(),
        threeLinesAdapterDelegate(),
        textIconAdapterDelegate(),
        switchAdapterDelegate(::changeCouponActiveStatus)
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        postponeEnterTransition()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            sharedElementEnterTransition = TransitionInflater.from(requireContext())
                .inflateTransition(android.R.transition.move)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_coupon_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        applyTransitionAnimations()
        initViews()
        observeLiveDatas()
        viewModel.getData(args.CouponID)
    }

    private fun applyTransitionAnimations() {
        val id = args.CouponID
        ViewCompat.setTransitionName(coupon_detail_parallax, "${id}_COUPON_BG")
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context as? AppCompatActivity)?.let {
            it.setSupportActionBar(coupon_detail_toolbar)
        }
    }

    private fun initViews() {
        coupon_detail_recycler.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        coupon_detail_recycler.addItemDecoration(VerticalSpaceItemDecoration(5))
        coupon_detail_recycler.adapter = adapter
    }

    private fun observeLiveDatas() {
        viewModel.couponDetailLiveData.observe(viewLifecycleOwner, Observer { data ->
            data?.either(this::handleError, this::handleSuccess)
        })
        viewModel.fullLoadingLiveData.observe(viewLifecycleOwner, Observer { data ->
            data?.let(::handleLoading)
        })
    }

    // Interactions

    private fun handleLoading(isLoading: Boolean) {
        coupon_detail_loading.visible(isLoading)
    }

    private fun handleSuccess(data: CouponDetailUi) {
        coupon_detail_toolbar.title = data.title
        coupon_detail_collapsing_toolbar.title = data.title
        coupon_detail_parallax.loadImage(data.image) { startPostponedEnterTransition() }
        adapter.items = data.items
        val controller = AnimationUtils.loadLayoutAnimation(coupon_detail_recycler.context, R.anim.layout_animation_fall_down)
        coupon_detail_recycler.layoutAnimation = controller
        coupon_detail_recycler.adapter?.notifyDataSetChanged()
        coupon_detail_recycler.scheduleLayoutAnimation()
    }

    private fun handleError(error: Throwable) {
        Log.e("Error", error.message, error)
    }

    // Actions
    private fun seeDetails(couponId: String) {
        //open description details view
    }

    private fun changeCouponActiveStatus(couponId: String, active: Boolean) {
        viewModel.switchActive(couponId, active)
    }
}
