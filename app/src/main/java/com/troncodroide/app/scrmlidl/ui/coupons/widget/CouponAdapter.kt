package com.troncodroide.app.scrmlidl.ui.coupons.widget

import android.view.View
import androidx.core.view.ViewCompat
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegate
import com.troncodroide.app.scrmlidl.R
import com.troncodroide.app.scrmlidl.framework.extensions.loadImage
import com.troncodroide.app.scrmlidl.ui.extensions.apply
import com.troncodroide.app.scrmlidl.ui.extensions.visible
import com.troncodroide.app.scrmlidl.ui.models.CouponItemUI
import com.troncodroide.app.scrmlidl.ui.models.EstableItem
import com.troncodroide.app.scrmlidl.ui.models.SwitchItemUI
import com.troncodroide.app.scrmlidl.ui.models.TextIconItemUI
import com.troncodroide.app.scrmlidl.ui.models.ThreeLinesItemUI
import com.troncodroide.app.scrmlidl.ui.models.TwoLinesItemUI
import kotlinx.android.synthetic.main.view_coupon_list_item.view.*
import kotlinx.android.synthetic.main.view_coupon_list_item.view.coupon_item_subtitle
import kotlinx.android.synthetic.main.view_coupon_list_item.view.coupon_item_title
import kotlinx.android.synthetic.main.view_icon_text_item.view.*
import kotlinx.android.synthetic.main.view_three_lines_item.view.*

fun couponAdapterDelegate(detailClick: (couponId: String, transitionViews: Array<Pair<View, String>>) -> Unit = { _, _ -> },
                          activeChanged: (couponId: String, enabled: Boolean) -> Unit = { _, _ -> }) =
    adapterDelegate<CouponItemUI, EstableItem>(R.layout.view_coupon_list_item) {
        var notifySwitchActions = true
        itemView.coupon_item_switch.setOnCheckedChangeListener { buttonView, isChecked ->
            itemView.coupon_item_active.visible(isChecked)
            if (notifySwitchActions) activeChanged(item.couponID, isChecked)

        }

        itemView.setOnClickListener {
            detailClick(item.couponID, arrayOf(
                itemView.coupon_item_header_image_background to itemView.coupon_item_header_image_background.transitionName
            ))
        }

        bind { payload ->
            ViewCompat.setTransitionName(itemView.coupon_item_header_image_background, item.couponID + "_COUPON_BG")
            with(item) {
                itemView.coupon_item_active.visible(isChecked)
                itemView.coupon_item_header_image_left.loadImage(productLogoImageUrl)
                itemView.coupon_item_header_image_right.loadImage(productImageUrl)
                itemView.coupon_item_header_image_background.loadImage(productBackground)
                itemView.coupon_item_product_title.text = productTitle
                itemView.coupon_item_product_subtitle.text = productSubtitle
                itemView.coupon_item_title.text = profitTitle
                itemView.coupon_item_subtitle.text = profitSubtitle
                notifySwitchActions = false
                itemView.coupon_item_switch.isChecked = isChecked
                notifySwitchActions = true
            }
        }
    }

fun twoLinesAdapterDelegate() =
    adapterDelegate<TwoLinesItemUI, EstableItem>(R.layout.view_two_lines_item) {
        bind { payload ->
            with(item) {
                itemView.coupon_item_title.apply(title)
                itemView.coupon_item_subtitle.apply(subtitle)
            }
        }
    }

fun threeLinesAdapterDelegate() =
    adapterDelegate<ThreeLinesItemUI, EstableItem>(R.layout.view_three_lines_item) {
        bind { payload ->
            with(item) {
                itemView.coupon_item_category.apply(category)
                itemView.coupon_item_title.apply(title)
                itemView.coupon_item_subtitle.apply(subtitle)
            }
        }
    }

fun textIconAdapterDelegate() =
    adapterDelegate<TextIconItemUI, EstableItem>(R.layout.view_icon_text_item) {
        bind { payload ->
            with(item) {
                itemView.coupon_item_text.apply(text)
                itemView.coupon_item_icon.setImageResource(icon)
            }
        }
    }

fun switchAdapterDelegate(activeChanged: (couponId: String, enabled: Boolean) -> Unit) =
    adapterDelegate<SwitchItemUI, EstableItem>(R.layout.view_switch_item) {
        bind { payload ->
            var notifySwitchActions = true
            itemView.coupon_item_switch.setOnCheckedChangeListener { buttonView, isChecked ->
                if (notifySwitchActions) activeChanged(item.couponID, isChecked)
            }
            with(item) {
                notifySwitchActions = false
                itemView.coupon_item_switch.isChecked = selected
                notifySwitchActions = true
                itemView.coupon_item_title.apply(title)
                itemView.coupon_item_subtitle.apply(subtitle)
            }
        }
    }