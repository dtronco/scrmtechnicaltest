package com.troncodroide.app.scrmlidl.ui.coupons

import com.troncodroide.app.scrmlidl.ui.models.EstableItem
import java.io.Serializable

data class CouponDetailUi(
    val couponID: String,
    val title: String,
    val image: String,
    val items: List<EstableItem>
) : Serializable