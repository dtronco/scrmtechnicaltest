package com.troncodroide.app.scrmlidl.ui.coupons

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.troncodroide.app.core.Either
import com.troncodroide.app.core.map
import com.troncodroide.app.scrmlidl.framework.providers.CalendarEavaluator
import com.troncodroide.app.scrmlidl.framework.providers.ResProvider
import com.troncodroide.app.scrmlidl.ui.mapper.UIMappers
import com.troncodroide.app.scrmlidl.usecases.GetCoupon
import com.troncodroide.app.scrmlidl.usecases.SetActiveCoupon
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class CouponDetailViewModel(private val goupons: GetCoupon, private val activeCoupon: SetActiveCoupon, private val resProvider: ResProvider, private val calendarEvaluator: CalendarEavaluator) : ViewModel() {
    val couponDetailLiveData: MutableLiveData<Either<Throwable, CouponDetailUi>?> = MutableLiveData()
    val fullLoadingLiveData: MutableLiveData<Boolean> = MutableLiveData()

    fun getData(couponId: String) = GlobalScope.launch(Dispatchers.Main) {
        fullLoadingLiveData.value = true
        val data = goupons(couponId)
        fullLoadingLiveData.value = false
        couponDetailLiveData.value = data.map { domainCoupon -> UIMappers.DetailMapper.mapDetailUI(resProvider, domainCoupon, calendarEvaluator) }
    }.start()

    fun switchActive(couponId: String, active: Boolean) = GlobalScope.launch(Dispatchers.Main) {
        activeCoupon(couponId to active)
    }.start()
}