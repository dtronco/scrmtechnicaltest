package com.troncodroide.app.scrmlidl.di

import com.troncodroide.app.scrmlidl.data.RepositoryImpl
import com.troncodroide.app.scrmlidl.data.contracts.MemDatasource
import com.troncodroide.app.scrmlidl.data.contracts.NetworkDatasource
import com.troncodroide.app.scrmlidl.data.contracts.PersistenceDatasource
import com.troncodroide.app.scrmlidl.framework.FakeNetworkFromAssetsDS
import com.troncodroide.app.scrmlidl.framework.MemCacheDS
import com.troncodroide.app.scrmlidl.framework.PersistenceDS
import com.troncodroide.app.scrmlidl.framework.providers.CalendarEavaluator
import com.troncodroide.app.scrmlidl.framework.providers.CalendarEvaluatorImpl
import com.troncodroide.app.scrmlidl.framework.providers.ResProvider
import com.troncodroide.app.scrmlidl.framework.providers.ResProviderImpl
import com.troncodroide.app.scrmlidl.ui.coupons.CouponDetailViewModel
import com.troncodroide.app.scrmlidl.ui.coupons.CouponListViewModel
import com.troncodroide.app.scrmlidl.usecases.GetCoupon
import com.troncodroide.app.scrmlidl.usecases.GetCoupons
import com.troncodroide.app.scrmlidl.usecases.SetActiveCoupon
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val data = module {
    single { RepositoryImpl(get(), get(), get()) }
}

val usecases = module {
    single { GetCoupon(get()) }
    single { GetCoupons(get()) }
    single { SetActiveCoupon(get()) }
}

val framework = module {
    single<MemDatasource> { MemCacheDS() }
    single<PersistenceDatasource> { PersistenceDS(androidContext()) }
    single<NetworkDatasource> { FakeNetworkFromAssetsDS(androidContext()) }
    single<ResProvider> { ResProviderImpl(androidContext()) }
    single<CalendarEavaluator> { CalendarEvaluatorImpl() }
}

val ui = module {
    viewModel { CouponListViewModel(get(), get(), get()) }
    viewModel { CouponDetailViewModel(get(), get(), get(), get()) }
}

val modules = listOf(framework, data, usecases, ui)
