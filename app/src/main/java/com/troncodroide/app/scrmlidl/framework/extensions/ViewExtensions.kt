package com.troncodroide.app.scrmlidl.framework.extensions

import android.widget.ImageView
import androidx.core.net.toUri
import com.google.firebase.storage.FirebaseStorage
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

fun ImageView.loadImage(url: String, callback: () -> Unit = {}) {

    if (url.toUri().scheme == "GS") {
        val uri = url.toUri()
        val storageRef = FirebaseStorage.getInstance().reference
        val imageRef = storageRef.child("/" + uri.host + uri.path!!)
        imageRef.downloadUrl.addOnCompleteListener { result ->
            val imageUri = result.result
            load(imageUri?.toString(), callback)
        }
    } else {
        load(url, callback)
    }
}

private fun ImageView.load(url: String?, callback: () -> Unit = {}) {
    Picasso.get().load(url).into(this, object : Callback {
        override fun onSuccess() {
            callback()
        }

        override fun onError(e: Exception?) {
            callback()
        }
    })
}
