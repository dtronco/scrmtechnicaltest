package com.troncodroide.app.scrmlidl.framework.models

import com.google.gson.annotations.SerializedName

data class Coupon(
    @SerializedName("id") val id: String,
    @SerializedName("productId") val productid: String,
    @SerializedName("productBrand") val productBrand: String,
    @SerializedName("productTitle") val productTitle: String,
    @SerializedName("productSubTitle") val productSubTitle: String,
    @SerializedName("productImage") val productImage: String,
    @SerializedName("productLogo") val productLogo: String,
    @SerializedName("productBackground") val productBackground: String,
    @SerializedName("profit") val profit: String,
    @SerializedName("startDate") val startDate: String,
    @SerializedName("endDate") val endDate: String,
    @SerializedName("enabled") val enabled: Boolean,
    @SerializedName("limitations") val limitations: String
)