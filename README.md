## Base Technical App

#### Knowledge
- kotlin
- Clean Architecture
- Core app
- Coroutines
- Firebase (Firestore)
- Adapter delegates
- CI (bitbucket pipelines)

#### Architecture
Based on clean architecture post from [Antonio Leiva](https://devexperto.com/clean-architecture-android/)

![Figure 1](/readme/images/clean-architecture-interaction.png)

#### Modules
There are two scopes on this project, the Android scope and the only kotlin scope.
The Android scope includes the framework and UI logic, all the packages and 
interfaces implementations that uses the Android libs, the only kotlin scope 
includes the data, domain and usecases modules. They are written only in java/kotlin language 
and those libraries are not coupled with the target platform. This means that these modules could be imported into other projects

##### Kotlin Android Modules
1. Core: This module is for general framework libs, this would be replaced 
with an artifact. Right now there are some classes that are for two scopes, 
Android, and Kotlin. This should be splitted into core-app (for Android scope) 
and core (for Kotlin scope)
2. App: 
This module includes all the application packages
    1. UI: Related to Activities Fragments, widgets and more ui
    2. Framework: The implementation of datasources interfaces (:data)
    3. DI: All the dependency injections (Koin)

##### Kotlin modules
This module include all the library modules that can be written only in kotlin/java. 
It is not coupled to the final application

1. Usecases: Bussines logic
2. data: Datasources contracts and repository contract and repository implementation
3. domain: All the domain classes