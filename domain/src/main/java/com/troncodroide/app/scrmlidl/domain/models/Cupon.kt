package com.troncodroide.app.scrmlidl.domain.models

import java.io.Serializable

data class Coupon(
    val id: String,
    val product: Product,
    val profit: Profit,
    val startDate: String,
    val endDate: String,
    val enabled: Boolean,
    val limitation: Limitation
) : Serializable

data class Product(
    val id: String,
    val title: String,
    val description: String,
    val image: String,
    val logo: String,
    val background: String
) : Serializable

data class Limitation(
    val items: Int?,
    val maxAmount: Amount?,
    val minAmount: Amount?,
    val maxUses: Int?
) : Serializable

data class Profit(
    val id: String,
    val unit: Int,
    val percentage: Float
) : Serializable

data class Amount(
    val money: Float,
    val currency: String
) : Serializable
