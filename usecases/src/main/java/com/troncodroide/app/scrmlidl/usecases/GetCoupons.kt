package com.troncodroide.app.scrmlidl.usecases

import com.troncodroide.app.core.Either
import com.troncodroide.app.core.corroutines.SimpleUseCase
import com.troncodroide.app.scrmlidl.data.RepositoryImpl
import com.troncodroide.app.scrmlidl.domain.models.Coupon

class GetCoupons(private val repositoryImpl: RepositoryImpl) : SimpleUseCase<Either<Throwable, List<Coupon>>>() {

    override fun run(): Either<Throwable, List<Coupon>> {
        return repositoryImpl.getData()
    }
}

