package com.troncodroide.app.scrmlidl.usecases

import com.troncodroide.app.core.Either
import com.troncodroide.app.core.corroutines.UseCase
import com.troncodroide.app.scrmlidl.data.RepositoryImpl
import com.troncodroide.app.scrmlidl.domain.models.Coupon

class GetCoupon(private val repositoryImpl: RepositoryImpl) : UseCase<String, Either<Throwable, Coupon>>() {

    override fun run(param: String): Either<Throwable, Coupon> {
        return repositoryImpl.getDetail(param)
    }
}

