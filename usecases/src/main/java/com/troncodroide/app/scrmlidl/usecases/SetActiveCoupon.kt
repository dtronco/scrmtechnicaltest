package com.troncodroide.app.scrmlidl.usecases

import com.troncodroide.app.core.Either
import com.troncodroide.app.core.corroutines.UseCase
import com.troncodroide.app.scrmlidl.data.RepositoryImpl

class SetActiveCoupon(private val repositoryImpl: RepositoryImpl) : UseCase<Pair<String, Boolean>, Either<Throwable, Unit>>() {

    override fun run(param: Pair<String, Boolean>): Either<Throwable, Unit> {
        return repositoryImpl.setActive(param.first, param.second)
    }
}

