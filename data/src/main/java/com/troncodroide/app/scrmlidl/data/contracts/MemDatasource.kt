package com.troncodroide.app.scrmlidl.data.contracts

import com.troncodroide.app.core.Either
import com.troncodroide.app.scrmlidl.domain.models.Coupon

interface MemDatasource {
    fun hasData(page: Int): Boolean
    fun getData(page: Int): Either<Throwable, List<Coupon>>
    fun setData(page: Int, data: List<Coupon>)
}