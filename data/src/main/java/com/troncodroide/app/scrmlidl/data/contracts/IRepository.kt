package com.troncodroide.app.scrmlidl.data.contracts

import com.troncodroide.app.core.Either
import com.troncodroide.app.scrmlidl.domain.models.Coupon

interface IRepository {
    fun getData(): Either<Throwable, List<Coupon>>
    fun getDetail(id: String): Either<Throwable, Coupon>
    fun setActive(id: String, active: Boolean): Either<Throwable, Unit>
}