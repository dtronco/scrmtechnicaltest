package com.troncodroide.app.scrmlidl.data

import com.troncodroide.app.core.Either
import com.troncodroide.app.core.either
import com.troncodroide.app.core.map
import com.troncodroide.app.scrmlidl.data.contracts.IRepository
import com.troncodroide.app.scrmlidl.data.contracts.MemDatasource
import com.troncodroide.app.scrmlidl.data.contracts.NetworkDatasource
import com.troncodroide.app.scrmlidl.data.contracts.PersistenceDatasource
import com.troncodroide.app.scrmlidl.domain.models.Coupon

class RepositoryImpl(private val networkDataSource: NetworkDatasource, private val memDatasource: MemDatasource, private val persistenceDataSource: PersistenceDatasource) : IRepository {
    override fun getDetail(id: String): Either<Throwable, Coupon> {
        val data = if (memDatasource.hasData(0)) {
            memDatasource.getData(0)
        } else {
            networkDataSource.getData(0)
        }
        data.either({}, { memDatasource.setData(0, it) })
        return data.map { list ->
            list.first { it.id == id }.copy(enabled = persistenceDataSource.isActive(id))
        }
    }

    override fun setActive(id: String, active: Boolean): Either<Throwable, Unit> {
        persistenceDataSource.setActive(id, active)
        return Either.Right(Unit)
    }

    override fun getData(): Either<Throwable, List<Coupon>> {
        val data = if (memDatasource.hasData(0)) {
            memDatasource.getData(0)
        } else {
            networkDataSource.getData(0)
        }
        data.either({}, { memDatasource.setData(0, it) })

        return data.map { list ->
            list.map { coupon -> coupon.copy(enabled = persistenceDataSource.isActive(coupon.id)) }
        }
    }
}
