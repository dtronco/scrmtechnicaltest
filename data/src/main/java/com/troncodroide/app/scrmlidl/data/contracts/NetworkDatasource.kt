package com.troncodroide.app.scrmlidl.data.contracts

import com.troncodroide.app.core.Either
import com.troncodroide.app.scrmlidl.domain.models.Coupon

interface NetworkDatasource {
    fun getData(page: Int): Either<Throwable, List<Coupon>>
}