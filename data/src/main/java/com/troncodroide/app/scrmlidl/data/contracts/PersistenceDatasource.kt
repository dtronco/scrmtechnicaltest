package com.troncodroide.app.scrmlidl.data.contracts

interface PersistenceDatasource {
    fun isActive(id: String): Boolean
    fun setActive(id: String, active: Boolean)
}