package com.troncodroide.app.core.corroutines

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

abstract class SimpleUseCase<out OUT> : UseCase<Unit, OUT>() {
    suspend operator fun invoke(): OUT = withContext(Dispatchers.IO) {
        run()
    }

    abstract fun run(): OUT

    override fun run(param: Unit): OUT {
        return run()
    }
}

abstract class UseCase<in IN, out OUT> {
    suspend operator fun invoke(param: IN): OUT = withContext(Dispatchers.IO) {
        run(param)
    }

    abstract fun run(param: IN): OUT
}
